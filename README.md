# M17-UF1-E1-MyVector-NolteJan

## 1 - Introducció: Hello {name}

Crear un programa que faci el següent:

“Hello {Name}”: funció que donat nom escrit per usuari retorni salutació amb el nom insertat.

## 2 - Classe MyVector

Crea una classe amb nom MyVector. Implenta els següent requisits:

- Guarda i gestiona vectors de dos dimensions
- Variable on es guarda les coordenades 2D d’un vector. Punt d’inici i punt final.
- Funcions get i set 
- Funcions que retornen la distancia del vector
- Funció que canvia la direcció del vector
- Funció .toString() 

## 3 - Classe VectorGame

Crea una segona classe anomenada VectorGame. Implementa els següents requisits:

- Funció “randomVectors”: retorna un array de MyVector on hi ha generat vectors aleatoris. Per paràmetre es passa la allargada de l’array
- Funció “SortVectors”: donat un array de MyVectors retorna aquest array segons la distància dels vectors o segons proximitat a l’origen. Arriba per paràmetre l’array de MyVectors i un booleà segons el tipus d’ordre.
- Treu per pantalla de manera clara (títol de cada prova) el resultat d’aquestes funcions. (Main Class)
- (Bonus) Crea una funció que representa visualment la direcció d’un vector (utilitzant caràcters a la consola) 

## 4 - Funcions a la classe Main

De forma modular i ordenada a la classe Main implementa la impressió per consola de totes les funcions demandes a l’enunciat.

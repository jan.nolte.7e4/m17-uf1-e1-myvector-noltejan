﻿using System;

namespace M17_UF1_E1_MyVector_NolteJan
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            string name;
            int selection;
            Console.Write("Introdueix el teu nom:\t");
            name = Console.ReadLine();
            Console.WriteLine("Hello {0} "+(char)9786, name);
            VectorGame game = new VectorGame();
            myVector[] arrayVectors = null;
            /*
            for(int x=0; x<10000; x++)
            {
                Console.WriteLine(x.ToString()+": "+((char)x).ToString());
            }*/
            do
            {
                Console.Write("MENU:\n 0: EXIT \n 1: Generació d'un array de vectors aleatoris.\n 2: Ordenació de l'array de vectors.\n 3: Mostra l'array per pantalla.\n");
                selection = readInt();
                switch (selection)
                {
                    case 0:
                        Console.WriteLine("S'esta sortint del programa.");
                        break;
                    case 1:
                        Console.WriteLine("Introdueix la quantita de vectors que vols que tingui el vector");
                        arrayVectors=game.randomVectors(readInt());
                        break;
                    case 2:
                        if (arrayVectors != null && arrayVectors.Length>0)
                        {
                            Console.WriteLine("Per ordenar per proximitat introdueix 1, per ordenar per longitud introdueix 2");
                            int sel = readInt();
                            Console.WriteLine("L'array de vectors ordenat és:");
                            switch (sel)
                            {
                                case 1:
                                    game.SortVectors(arrayVectors, true, false);
                                    break;
                                case 2:
                                    game.SortVectors(arrayVectors, false, true);
                                    break;
                                default:
                                    Console.WriteLine("Valor introduit incorrecte.");
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("No hi ha cap array que ordenar");
                        }
                        break;
                    case 3:
                        if(arrayVectors!=null && arrayVectors.Length > 0)
                        {
                            game.showArray(arrayVectors, true);
                        }
                        else
                        {
                            Console.WriteLine("No hi ha cap array que mostrar");
                        }
                        break;
                    default:
                        Console.WriteLine("Valor de selecció es incorrecte.");
                        break;
                }
            } while (selection!=0);
        }
        private static int readInt()
        {
            string inputText;
            while (true)
            {
                Console.WriteLine("Introdueix un valor numeric:\t");
                inputText = Console.ReadLine();
                if (checkNumbers(inputText))
                {
                    return int.Parse(inputText);
                }
                Console.WriteLine("El valor d'entrada ha de ser numeric.");
            }
            
        }
        private static bool checkNumbers(string inputString)
        {
            if (inputString == null || inputString=="")
            {
                return false;
            }
            foreach(char c in inputString)
            {
                if(c < '0' || c > '9')
                {
                    return false;
                }
            }
            return true;
        }
    }
}

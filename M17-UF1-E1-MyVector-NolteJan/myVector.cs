﻿using System;

namespace M17_UF1_E1_MyVector_NolteJan
{
    class myVector
    {
        public int[] puntInicial { get; set; }
        public int[] puntFinal { get; set; }
        /*public myVector()
        {

        }*/
        public myVector(int[] PInicial, int[] PFinal)
        {
            puntInicial = PInicial;
            puntFinal = PFinal;
        }
        public myVector(int[] PFinal)
        {
            puntInicial = new int[] { 0, 0 };
            puntFinal = PFinal;
        }
        /*
        public int[] getpuntInicial()
        {
            return puntInicial;
        }
        public int[] getpuntFinal()
        {
            return puntFinal;
        }
        public void setpuntInicial(int[] PInicial)
        {
            puntInicial = PInicial;
        }
        public void setpuntFinal(int[] PFinal)
        {
            puntFinal = PFinal;
        }
        */
        public double lengthVector()
        {
            return Math.Sqrt(Math.Pow(Math.Abs(puntFinal[0] - puntInicial[0]), 2) + Math.Pow(Math.Abs(puntFinal[1] - puntInicial[1]), 2));
        }
        /*public double lengthVector(int[] vectorInici, int[] vectorFinal)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(vectorFinal[0] - vectorInici[0]), 2) + Math.Pow(Math.Abs(vectorFinal[1] - vectorInici[1]), 2));
        }*/
        public double distOrigen()
        {
            /*
             * La distancia fins al punt d'origen l'obtinc de la disctancia mes curta entre qualsevol dels dos punts fins el punt {0,0}
            */
            double distInici = Math.Sqrt(Math.Pow(Math.Abs(puntInicial[0]), 2) + Math.Pow(Math.Abs(puntInicial[1]), 2));
            double distFinal = Math.Sqrt(Math.Pow(Math.Abs(puntFinal[0]), 2) + Math.Pow(Math.Abs(puntFinal[1]), 2));
            if (distInici > distFinal)
            {
                return distFinal;
            }
            else
            {
                return distInici;
            }

        }

        public void changeDirec()
        {
            int[] tempInicial = puntInicial;
            puntInicial = new int[] { puntFinal[0], puntFinal[1] };
            puntFinal = new int[] { tempInicial[0], tempInicial[1] };
        }
        public override string ToString()
        {
            return "Punt d'inici del vector {" + puntInicial[0] + ", " + puntInicial[1] + "}, punt final del vector {" + puntFinal[0] + ", " + puntFinal[1] + "}";
        }
        public static int CompareByProximity(myVector vector1, myVector vector2)
        {
            return vector1.distOrigen().CompareTo(vector2.distOrigen());
        }
        public static int CompareByLength(myVector vector1, myVector vector2)
        {
            return vector1.lengthVector().CompareTo(vector2.lengthVector());
        }
        public string direccioVector()
        {
            int[] newPuntFinal = new int[] { puntFinal[0] - puntInicial[0], puntFinal[1] - puntInicial[1] };
            if (newPuntFinal[0] < 0)
            {
                if (newPuntFinal[1] < 0)
                {
                    return "Direcció:\n\t  /\n\t /\n\tX\n";
                }
                else if (newPuntFinal[1] == 0)
                {
                    return "Direcció:\n\n\t"+(char)9668+"--\n\n";
                }
                else if (newPuntFinal[1] > 0)
                {
                    return "Direcció:\n\tX\n\t \\\n\t  \\\n";
                }
            }
            else if (newPuntFinal[0] == 0)
            {
                if (newPuntFinal[1] < 0)
                {
                    return "Direcció:\n\t |\n\t |\n\t " + (char)9660 + "\n";
                }
                else if (newPuntFinal[1] == 0)
                {
                    return "Direcció:\n\n\t  X\n\n";
                }
                else if (newPuntFinal[1] > 0)
                {
                    return "Direcció:\n\t " + (char)9650 + "\n\t |\n\t |\n";
                }
            }
            else if (newPuntFinal[0] > 0)
            {
                if (newPuntFinal[1] < 0)
                {
                    return "Direcció:\n\t\\\n\t \\\n\t  X\n";
                }
                else if (newPuntFinal[1] == 0)
                {
                    return "Direcció:\n\n\t--" + (char)9658 + "\n\n";
                }
                else if (newPuntFinal[1] > 0)
                {
                    return "Direcció:\n\t  X\n\t /\n\t/\n";
                }
            }
            return "Error: direccio no trobada.";
        }
    }
}

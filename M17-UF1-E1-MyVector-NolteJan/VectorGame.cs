﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_NolteJan
{
    class VectorGame
    {
        public VectorGame()
        {

        }
        public myVector[] randomVectors(int numVectors)
        {
            myVector[] arrayVectors = new myVector[numVectors];
            for (int x = 0; x < arrayVectors.Length; x++)
            {
                arrayVectors[x] = new myVector(randomVector(20), randomVector(20));
            }
            showArray(arrayVectors, false);
            return arrayVectors;
        }
        private int[] randomVector(int max)
        {
            Random randomGen = new Random();
            return new int[] { randomGen.Next(-max, max), randomGen.Next(-max, max) };
        }
        public myVector[] SortVectors(myVector[] arrayVectors, bool proximitat, bool longitud)
        {
            showArray(arrayVectors, false);
            if (proximitat)
            {
                Array.Sort(arrayVectors, myVector.CompareByProximity);
            }else if (longitud)
            {
                Array.Sort(arrayVectors, myVector.CompareByLength);
            }
            showArray(arrayVectors, false);
            return arrayVectors;
        }
        public void showArray(myVector[] arrayVectors, bool complet)
        {
            int y = 0;
            Console.WriteLine("L'array de vectors és:\n");
            foreach (myVector vector in arrayVectors)
            {
                y++;
                Console.Write("Vector {0}:\t{1}", y, vector.ToString());
                if (complet)
                {
                    Console.Write("\n{0}Proximitat:\t{1}\nLongitud:\t{2}\n------------------------------------------------------------------------",vector.direccioVector(),  vector.distOrigen(), vector.lengthVector());
                }
                Console.WriteLine();
            }
        }
    }
}
